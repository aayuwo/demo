package com.jtzh.framework.aspectj.lang.enums;

/**
 * 操作状态
 *
 * @author jtzh
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
