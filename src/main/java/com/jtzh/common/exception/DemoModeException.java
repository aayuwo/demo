package com.jtzh.common.exception;

/**
 * 演示模式异常
 *
 * @author jtzh
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
